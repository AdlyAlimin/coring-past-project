<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>MyDipp Dashboard</title>

<!-- vendor css -->
<link href="lib/fontawesome-free/css/all.min.css" rel="stylesheet">
<link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
<link href="lib/typicons.font/typicons.css" rel="stylesheet">
<link href="lib/morris.js/morris.css" rel="stylesheet">
<link href="lib/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
<link href="lib/jqvmap/jqvmap.min.css" rel="stylesheet">
<link href="lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="lib/select2/css/select2.min.css" rel="stylesheet">
<link href="lib/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="lib/sweetalert2/dist/sweetalert2.all.min.js"></script>
<!-- azia CSS -->
<link rel="stylesheet" href="css/azia.css">