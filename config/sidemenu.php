<div class="az-sidebar">
    <div class="az-sidebar-header">
        <a href="home.php" class="az-logo">My<span>Dipp</span></a>
    </div><!-- az-sidebar-header -->
    <div class="az-sidebar-body">
        <ul class="nav">            
            <li class="nav-label">Menu Utama</li>

            <li class="nav-item active">
                <a href="home.php" class="nav-link"><i class="typcn typcn-book"></i>Utama</a>
            </li><!-- nav-item -->

            <li class="nav-item">
                <a href="" class="nav-link with-sub"><i class="typcn typcn-clipboard"></i>Makanan</a>
                <nav class="nav-sub">
                    <a href="meals.php?category=fish" class="nav-sub-link">Makanan Laut</a>
                    <a href="meals.php?category=meat" class="nav-sub-link">Daging</a>
                    <a href="meals.php?category=bread" class="nav-sub-link">Roti</a>
                    <a href="meals.php?category=fat" class="nav-sub-link">Lemak</a>
                    <a href="meals.php?category=fruit" class="nav-sub-link">Buah-Buahan</a>
                    <a href="meals.php?category=legume" class="nav-sub-link">Kekacang</a>
                    <a href="meals.php?category=milk" class="nav-sub-link">Susu</a>
                    <a href="meals.php?category=rice" class="nav-sub-link">Beras & Bijiran</a>
                    <a href="meals.php?category=sweet" class="nav-sub-link">Manisan</a>
                    <a href="meals.php?category=vege" class="nav-sub-link">Sayur-Sayuran</a>
                    <a href="meals.php?category=other" class="nav-sub-link">Makanan Lain</a>
                </nav>
            </li><!-- nav-item -->

            <li class="nav-item">
                <a href="exercise.php" class="nav-link"><i class="fas fa-walking"></i>Senaman</a>
            </li><!-- nav-item -->

            <li class="nav-item">
                <a href="group.php" class="nav-link"><i class="typcn typcn-group"></i>Kumpulan</a>
            </li><!-- nav-item -->

            <li class="nav-item">
                <a href="logout.php" class="nav-link"><i class="typcn typcn-power"></i>Daftar Keluar</a>
            </li><!-- nav-item -->
        </ul><!-- nav -->
    </div><!-- az-sidebar-body -->
</div><!-- az-sidebar -->