-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2019 at 12:34 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coring`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `name`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `bmi`
--

CREATE TABLE `bmi` (
  `username` varchar(250) NOT NULL,
  `gender` varchar(20) DEFAULT 'None',
  `weight` int(20) DEFAULT '0',
  `height` int(20) DEFAULT '0',
  `bmi` float(4,1) DEFAULT '0.0',
  `age` int(20) DEFAULT '0',
  `update_date` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmi`
--

INSERT INTO `bmi` (`username`, `gender`, `weight`, `height`, `bmi`, `age`, `update_date`) VALUES
('asd', 'Male', 90, 154, 37.9, 25, '10-04-2019'),
('Lkj', 'None', 0, 0, 0.0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bread`
--

CREATE TABLE `bread` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bread`
--

INSERT INTO `bread` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Onion Bagel', '1 Piece', 306),
(2, 'Kaya Bun', '1 Piece', 549),
(3, 'Wholemeal Bun', '1 Piece', 54),
(4, 'Hard Roll', '1 Piece', 103),
(5, 'Foccaccia Bread', '1 Slice', 220),
(6, 'Sausage Roll', '1 Piece', 184),
(7, 'Croissant', '1 Piece', 215),
(8, 'Blueberry Danish', '1 Piece', 145),
(9, 'Custard Peach Danish', '1 Piece', 182);

-- --------------------------------------------------------

--
-- Table structure for table `coach`
--

CREATE TABLE `coach` (
  `username` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gender` varchar(25) DEFAULT NULL,
  `position` varchar(150) DEFAULT NULL,
  `age` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coach`
--

INSERT INTO `coach` (`username`, `email`, `password`, `gender`, `position`, `age`) VALUES
('Test', 'test@test', 'test', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_activity`
--

CREATE TABLE `exercise_activity` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(250) NOT NULL,
  `activity` varchar(250) NOT NULL,
  `caloriesBurn` varchar(250) NOT NULL,
  `activityTime` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise_activity`
--

INSERT INTO `exercise_activity` (`id`, `username`, `time`, `type`, `activity`, `caloriesBurn`, `activityTime`) VALUES
(1, 'asd', '2019-04-03 07:18:15', 'Bakery', 'General', '30', '5'),
(14, 'asd', '2019-04-04 04:34:04', 'Pilates', 'General', '45', '10'),
(15, 'asd', '2019-04-04 06:05:22', 'Boxing', 'General', '192', '10'),
(16, 'asd', '2019-04-06 08:53:59', 'Bakery', 'General', '60', '10'),
(17, 'asd', '2019-04-06 08:54:12', 'Badminton', 'General', '66', '8'),
(18, 'asd', '2019-04-07 09:44:34', 'Basketball', 'General', '98', '10'),
(19, 'asd', '2019-04-10 07:26:50', 'Bakery', 'General', '60', '10'),
(20, 'asd', '2019-04-10 07:27:02', 'Climbing Hills', 'General', '98', '10'),
(21, 'asd', '2019-04-10 07:27:15', 'Dancing', 'General', '110', '10'),
(22, 'asd', '2019-04-23 02:59:45', 'Bicycling', 'General', '68', '6'),
(23, 'asd', '2019-05-09 07:45:37', 'Bakery', 'General', '42', '7'),
(24, 'asd', '2019-05-23 02:30:27', 'Bicycling', 'General', '68', '6'),
(2, 'Lkj', '2019-04-03 08:25:35', 'Bakery', 'General', '60', '10');

-- --------------------------------------------------------

--
-- Table structure for table `fat`
--

CREATE TABLE `fat` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fat`
--

INSERT INTO `fat` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Grated Coconut', '1 Dessert Spoon', 27),
(2, 'Marjerin', '1 Tea Spoon', 44),
(3, 'Mayoninnaise', '1 Tea Spoon', 45),
(4, 'Peanut Butter', '1 Tea Spoon', 66),
(5, 'Palm Olien', '1 Tea Spoon', 45),
(6, 'Ghee - Minyak Sapi', '1 Tea Spoon', 45),
(7, 'Coconut Milk', '1/4 Cup', 50),
(8, 'Coconut Milk Powder', '1 Dessert Spoon', 3),
(9, 'Peanut', '1/3 Cup', 65),
(10, 'Butter', '1 Small Slice', 45);

-- --------------------------------------------------------

--
-- Table structure for table `fish`
--

CREATE TABLE `fish` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fish`
--

INSERT INTO `fish` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Fish Ball', '1 Piece', 5),
(2, 'Dried Anchovy', '1/3 Cup', 30),
(3, 'Indian Mackerel - Kembong', '1 Small Piece', 41),
(4, 'Spanish Mackerel - Tengiri', '1/2 Piece', 37),
(5, 'Cockles Boiled', '1/2 Cup', 49),
(6, 'Cuttlefish', '1 Piece', 33),
(7, 'Pink Prawn', '1 Piece', 3),
(8, 'Black Pomfret - Bawal Hitam', '1 Piece', 33),
(9, 'White Pomfret - Bawal Putih', '1 Piece', 51),
(10, 'Hairtail Scad - Cincaru', '1 Piece', 82),
(11, 'Japanese threadfin bream - Kerisi', '1 Piece', 45),
(12, 'Sardine', '1 Piece', 43),
(13, 'Yellow Banded Trevally - Selar', '1 Piece', 89);

-- --------------------------------------------------------

--
-- Table structure for table `fruits`
--

CREATE TABLE `fruits` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fruits`
--

INSERT INTO `fruits` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Grape', '1 Piece', 28),
(2, 'Star Fruit', '1 Whole Piece', 36),
(3, 'Papaya', '1 Slice', 74),
(4, 'Chestnuts', '1 Piece', 18),
(5, 'Date', '1 Piece', 21),
(6, 'Ciku / Sapodilla', '1 Piece', 67),
(7, 'Duku', '1 Piece', 17),
(8, 'Durian', '1 Piece', 28),
(9, 'Red Apple', '1 Piece', 64),
(10, 'Guava', '1/2 Piece', 69),
(11, 'Raisin', '1 Spoon', 62),
(12, 'Pear', '1 Piece', 67),
(13, 'Mandarin', '1 Piece', 73),
(14, 'Orange', '1 Piece', 71),
(15, 'Mango', '1 Piece', 73),
(16, 'JackFruit', '1 Piece', 19),
(17, 'Pineapple', '1 Piece', 64),
(18, 'Green Pear', '1 Piece', 61),
(19, 'Banana - Pisang Berangan', '1 Piece', 65),
(20, 'Banana - Pisang Mas', '1 Piece', 33),
(21, 'Prune', '1 Piece', 21),
(22, 'Rambutan', '1 Piece', 10),
(23, 'Watermalon', '1 Slice', 70),
(24, 'Honeydew', '1 Slice', 63),
(25, 'Kiwi', '1 Piece', 77),
(26, 'Dried Apricot', '1 Small Piece', 15),
(27, 'Unsalted Pistachio', '1/2 Cup', 59),
(28, 'Dragon Fruit', '1 Slice', 22),
(29, 'Pumpkin', '1 Cup Dice', 65),
(30, 'Sweet Potato', '1/2 Cup', 66),
(31, 'Potato', '1 Piece', 72);

-- --------------------------------------------------------

--
-- Table structure for table `group_category`
--

CREATE TABLE `group_category` (
  `id` int(50) NOT NULL,
  `g_name` varchar(250) NOT NULL,
  `g_desc` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_category`
--

INSERT INTO `group_category` (`id`, `g_name`, `g_desc`, `category`) VALUES
(2, 'qwe', 'qwe', 'qwe');

-- --------------------------------------------------------

--
-- Table structure for table `legume`
--

CREATE TABLE `legume` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legume`
--

INSERT INTO `legume` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Chinese Chestnut', '1 Piece', 13),
(2, 'Yellow Dal', '1/4 Cup', 121),
(3, 'Yellow Dal', '1/2 Cup', 121),
(4, 'Dried kidney Bean', '1/4 Cup', 110),
(5, 'Soyabean Curd', '1/2 Piece', 72),
(6, 'Mung Beans', '1/3 Cup', 141),
(7, 'Mung Beans', '3/4 Cup', 141),
(8, 'Chickpea', '1/4 Cup', 124),
(9, 'Chickpea', '1/2 Cup', 124),
(10, 'Red Beans', '1/4 Cup', 151),
(11, 'Baked Beans', '3/4 Cup', 135),
(12, 'Lotus Seed', '1 Piece', 4),
(13, 'Soyabean Milk Packet', '1 Box', 170),
(14, 'Soyabean Milk Unsweetened', '2/3 Glass', 112),
(15, 'Soybean Curd Unsweetened', '1 Container', 96),
(16, 'Egg Touhoo', '2/3 Cylinder', 65),
(17, 'Tau-kua (Tauhu)', '1 Piece', 65),
(18, 'Tempeh', '1 Piece', 70);

-- --------------------------------------------------------

--
-- Table structure for table `meals`
--

CREATE TABLE `meals` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `meals` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `menu` varchar(20) NOT NULL,
  `quantity` varchar(250) NOT NULL,
  `energy` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meals`
--

INSERT INTO `meals` (`id`, `username`, `time`, `meals`, `type`, `menu`, `quantity`, `energy`) VALUES
(19, 'asd', '2019-03-30 08:17:40', 'Lunch', 'rice', 'Kuey-teow', '2', 132),
(20, 'asd', '2019-03-31 03:27:30', 'Lunch', 'Rice', 'Rice Porridge', '2', 130),
(22, 'asd', '2019-03-31 03:27:50', 'Lunch', 'Sweet', 'Milk Chocolate Beans', '3', 9),
(23, 'asd', '2019-03-31 03:28:02', 'Lunch', 'Lagume', 'Chinese Chestnut', '6', 78),
(24, 'asd', '2019-04-04 06:05:38', 'Breakfast', 'Fruit', 'Red Apple', '3', 192),
(25, 'asd', '2019-04-04 06:05:48', 'Dinner', 'Fruit', 'Star Fruit', '3', 108),
(26, 'asd', '2019-04-04 06:06:03', 'Breakfast', 'Rice', 'Oats', '4', 296),
(27, 'asd', '2019-04-04 07:15:17', 'Dinner', 'Lagume', 'Chickpea', '9', 1116),
(28, 'asd', '2019-04-04 07:15:44', 'Dinner', 'Lagume', 'Dried kidney Bean', '1', 110),
(29, 'asd', '2019-04-04 07:16:00', 'Dinner', 'Rice', 'Filled Bun', '9', 1557),
(30, 'asd', '2019-04-07 08:10:31', 'Dinner', 'Bread', 'Hard Roll', '3', 309),
(31, 'asd', '2019-04-07 09:44:48', 'Breakfast', 'Bread', 'Hard Roll', '1', 103),
(32, 'asd', '2019-04-08 07:45:28', 'Dinner', 'Rice', 'Kuey-teow', '9', 594),
(33, 'asd', '2019-04-08 07:45:40', 'Dinner', 'Bread', 'Sausage Roll', '2', 368),
(34, 'asd', '2019-04-10 07:26:35', 'Breakfast', 'Fruit', 'Red Apple', '25', 1600),
(35, 'asd', '2019-04-23 03:26:40', 'Dinner', 'Bread', 'Onion Bagel', '3', 918),
(36, 'asd', '2019-05-09 07:45:49', 'Breakfast', 'Fruit', 'Star Fruit', '2', 72),
(37, 'asd', '2019-05-09 07:46:00', 'Dinner', 'Lagume', 'Yellow Dal', '2', 242),
(38, 'asd', '2019-05-09 07:46:21', 'Lunch', 'Meat', 'Beef', '9', 306),
(39, 'asd', '2019-05-23 02:24:19', 'Breakfast', 'Fruit', 'Star Fruit', '1', 36),
(40, 'asd', '2019-05-23 02:25:09', 'Dinner', 'Rice', 'Cornflakes', '2', 126),
(41, 'asd', '2019-05-23 03:54:09', 'Lunch', 'Rice', 'Rice Porridge', '50', 3250),
(42, 'asd', '2019-05-25 03:40:26', 'Lunch', 'Fish', 'Cockles Boiled', '3', 147),
(21, 'Lkj', '2019-03-31 03:27:39', 'Lunch', 'Lagume', 'Yellow Dal', '2', 242);

-- --------------------------------------------------------

--
-- Table structure for table `meat`
--

CREATE TABLE `meat` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meat`
--

INSERT INTO `meat` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Chicken Breast', '1 Piece', 57),
(2, 'Beef', '1 Match Box Size', 34),
(3, 'Chicken Liver', '1 Piece', 42),
(4, 'Chicken Wing', '1 Piece', 92),
(5, 'Hen Egg', '1 Piece', 84),
(6, 'Quail Egg', '1 Piece', 16),
(9, 'Chicken Thigh', '1 Piece', 293),
(10, 'Chicken Drummet', '1 Piece', 101),
(11, 'Chicken Drumstick', '1 Piece', 170);

-- --------------------------------------------------------

--
-- Table structure for table `milk`
--

CREATE TABLE `milk` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk`
--

INSERT INTO `milk` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Cheese', '1 Piece', 53),
(2, 'Cow Milk Full Cream', '1 Glass', 163),
(3, 'Evaporated Milk', '2/3 Cup', 155),
(4, 'Full Cream Milk', '1 Dessert Spoon', 36),
(5, 'Skim Milk Powder', '1 Dessert Spoon', 29),
(6, 'UHT Low Fat Milk', '1/2 Glass', 100),
(7, 'Fruit flavored yogurt', '1 Pot', 70),
(8, 'Full Cream Yogurt', '1 Pot', 70),
(9, 'Low Fat Yogurt', '3/4 Cup', 99);

-- --------------------------------------------------------

--
-- Table structure for table `noti`
--

CREATE TABLE `noti` (
  `username` varchar(25) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `module1` varchar(25) NOT NULL DEFAULT 'Yes',
  `module2` varchar(25) NOT NULL DEFAULT 'No',
  `module3` varchar(25) NOT NULL DEFAULT 'No',
  `module4` varchar(25) NOT NULL DEFAULT 'No',
  `module5` varchar(25) NOT NULL DEFAULT 'No',
  `module6` varchar(25) NOT NULL DEFAULT 'No',
  `module7` varchar(25) NOT NULL DEFAULT 'No',
  `module8` varchar(25) NOT NULL DEFAULT 'No',
  `module9` varchar(25) NOT NULL DEFAULT 'No',
  `module10` varchar(25) NOT NULL DEFAULT 'No',
  `module11` varchar(25) NOT NULL DEFAULT 'No',
  `module12` varchar(25) NOT NULL DEFAULT 'No',
  `module13` varchar(25) NOT NULL DEFAULT 'No',
  `module14` varchar(25) NOT NULL DEFAULT 'No',
  `module15` varchar(25) NOT NULL DEFAULT 'No',
  `module16` varchar(25) NOT NULL DEFAULT 'No',
  `module17` varchar(25) NOT NULL DEFAULT 'No',
  `module18` varchar(25) NOT NULL DEFAULT 'No',
  `module19` varchar(25) NOT NULL DEFAULT 'No',
  `module20` varchar(25) NOT NULL DEFAULT 'No',
  `module21` varchar(25) NOT NULL DEFAULT 'No',
  `module22` varchar(25) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noti`
--

INSERT INTO `noti` (`username`, `start_date`, `end_date`, `module1`, `module2`, `module3`, `module4`, `module5`, `module6`, `module7`, `module8`, `module9`, `module10`, `module11`, `module12`, `module13`, `module14`, `module15`, `module16`, `module17`, `module18`, `module19`, `module20`, `module21`, `module22`) VALUES
('asd', '2019-05-25', '2019-05-26', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
('Lkj', NULL, NULL, 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `other_meal`
--

CREATE TABLE `other_meal` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other_meal`
--

INSERT INTO `other_meal` (`id`, `name`, `measure`, `energy`) VALUES
(3, 'asd', 'w23', 23);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `username` varchar(250) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`username`, `name`, `role`) VALUES
('asd', 'Adly', 'User'),
('Lkj', NULL, 'User'),
('Test', 'test', 'Coach');

-- --------------------------------------------------------

--
-- Table structure for table `rice_cereal`
--

CREATE TABLE `rice_cereal` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rice_cereal`
--

INSERT INTO `rice_cereal` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Barley', '1/4 Cup', 70),
(2, 'Barley', '3/4 Cup', 70),
(3, 'Rice Porridge', '1 Cup', 65),
(4, 'Cornflakes', '1/2 Cup', 63),
(5, 'Kuey-teow', '1/2 Cup', 66),
(6, 'Macaroni', '1/4 Cup', 70),
(7, 'Macaroni', '2/3 Cup', 70),
(8, 'Noodle', '1/2 Cup', 68),
(9, 'Mi-hoon', '3/4 Cup', 69),
(10, 'Laksa', '2/3 Cup', 61),
(12, 'Rice', '2/3 Cup, 1/3 Bowl', 65),
(13, 'Oats', '1/2 Cup', 74),
(14, 'Putu Mayam', '1 Piece', 76),
(15, 'Cream Crackers', '1 Piece', 30),
(16, 'Small Cream Crackers', '1 Piece', 15),
(17, 'Biscuit Marie', '1 Piece', 29),
(18, 'French Bread', '1 Piece', 40),
(19, 'Bread', '1 Piece', 74),
(20, 'Wholemeal Bread', '1 Piece', 72),
(21, 'Filled Bun', '1 Piece', 173),
(22, 'Filled Bread with Cream', '1 Piece', 179),
(23, 'Crispy Cereal with Chocolate flavor', '1 Cup', 24),
(24, 'Honey Star Cereal', '1 Cup', 82),
(25, 'Chocolate Chip Biscuit', '1 Piece', 57);

-- --------------------------------------------------------

--
-- Table structure for table `sweet`
--

CREATE TABLE `sweet` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sweet`
--

INSERT INTO `sweet` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Sugar Cane Juice', '1/3 from 250ml', 61),
(2, 'Milk Chocolate Beans', '1 Piece', 3),
(3, 'Chocolate Raisin', '1 Piece', 11),
(4, 'Chocolate Milk', '1 Piece', 149),
(5, 'Chocolate Wafer Cylinder', '1 Stick', 22),
(6, 'Bworn Sugar', '1 Dessert Spoon', 64),
(7, 'Granulated Sugar', '1 Cube', 20),
(8, 'Granulated Sugar - Sand', '1 Dessert Spoon', 60),
(9, 'Pineapple jam', '1 Dessert Spoon', 61),
(10, 'Honey', '1 Dessert Spoon', 30),
(11, 'Chocolate Powder', '1 Dessert Spoon', 30),
(12, 'Malted Milk Drink', '1 Dessert Spoon', 42),
(13, 'Kaya', '1 Dessert Spoon', 68),
(14, 'Condensed Milk', '1 Dessert Spoon', 30),
(15, 'Teh Tarik with Sweetened Creamer', '1/3 Mug', 90),
(16, 'Chocolate Flavored Drink', '1/3 Mug', 91),
(17, 'Coffee with Sugar', '1/4 Mug', 60),
(18, 'Coffee with Sweetened Creamer', '1/4 Mug', 90),
(19, 'Tea with Sugar', '1/3 Mug', 60),
(20, 'Syrup', '1/3 Glass', 90),
(21, 'Orange Flavored Drink', '1/2 Glass', 60),
(22, 'Isotonic Drink', '1 Glass', 60);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `email`, `password`) VALUES
('asd', 'asd@asd', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `vege`
--

CREATE TABLE `vege` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `measure` varchar(20) NOT NULL,
  `energy` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vege`
--

INSERT INTO `vege` (`id`, `name`, `measure`, `energy`) VALUES
(1, 'Jantung Pisang', '1/4 Whole', 32),
(2, 'Jering Muda', '1 Piece', 13),
(3, 'Jering Tua', '1 Piece', 13),
(4, 'Kacang Botor', '1 Piece', 2),
(5, 'Kangkung', '1 piece', 1),
(6, 'Pegaga', '1 Bunch', 7),
(7, 'Pegaga Gajah', '1 Bunch', 6),
(8, 'Petai', '1', 3),
(9, 'Pucuk Ubi', '12 Piece', 6),
(10, 'Daun Selom', '10 Piece', 4),
(11, 'Terung Belanda', '1 Piece', 54),
(12, 'Ulam Raja', '10 Piece', 2),
(13, 'Cabbage', '1 Cup', 32),
(14, 'Carrot', '1 Cup', 15),
(16, 'Mustard leaves Chinese', '1 Cup', 9),
(17, 'Yam bean - Sengkuang', '1 Cup', 25);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `bmi`
--
ALTER TABLE `bmi`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `bread`
--
ALTER TABLE `bread`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `coach`
--
ALTER TABLE `coach`
  ADD PRIMARY KEY (`username`,`email`);

--
-- Indexes for table `exercise_activity`
--
ALTER TABLE `exercise_activity`
  ADD PRIMARY KEY (`username`,`time`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `fat`
--
ALTER TABLE `fat`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `fish`
--
ALTER TABLE `fish`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `fruits`
--
ALTER TABLE `fruits`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `group_category`
--
ALTER TABLE `group_category`
  ADD PRIMARY KEY (`id`,`g_name`,`category`);

--
-- Indexes for table `legume`
--
ALTER TABLE `legume`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `meals`
--
ALTER TABLE `meals`
  ADD PRIMARY KEY (`username`,`time`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `meat`
--
ALTER TABLE `meat`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `milk`
--
ALTER TABLE `milk`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `noti`
--
ALTER TABLE `noti`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `other_meal`
--
ALTER TABLE `other_meal`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `rice_cereal`
--
ALTER TABLE `rice_cereal`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `sweet`
--
ALTER TABLE `sweet`
  ADD PRIMARY KEY (`id`,`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`,`email`);

--
-- Indexes for table `vege`
--
ALTER TABLE `vege`
  ADD PRIMARY KEY (`id`,`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bread`
--
ALTER TABLE `bread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `exercise_activity`
--
ALTER TABLE `exercise_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `fat`
--
ALTER TABLE `fat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fish`
--
ALTER TABLE `fish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `fruits`
--
ALTER TABLE `fruits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `group_category`
--
ALTER TABLE `group_category`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `legume`
--
ALTER TABLE `legume`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `meals`
--
ALTER TABLE `meals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `meat`
--
ALTER TABLE `meat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `milk`
--
ALTER TABLE `milk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `other_meal`
--
ALTER TABLE `other_meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rice_cereal`
--
ALTER TABLE `rice_cereal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `sweet`
--
ALTER TABLE `sweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `vege`
--
ALTER TABLE `vege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
