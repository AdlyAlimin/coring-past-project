<?php 
session_start();
include("process/inc_db.php");
if(!isset($_SESSION['user'])){
  header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("config/head.php"); ?>
  </head>
  <body class="az-body az-body-sidebar">

    <?php include("config/sidemenu.php"); ?>

    <div class="az-content az-content-dashboard-two">

      <div class="az-content-header d-block d-md-flex">
        <div>
          <h2 class="az-content-title mg-b-5 mg-b-lg-8">Hi, Selamat Kembali <?= $_SESSION['user'] ?>!</h2>
          <p class="mg-b-0">Anda Sedang Melihat Papan Pemuka.</p>
        </div>
      </div><!-- az-content-header -->

      <div class="az-content-body">
        <hr>
        <?php
            if(isset($_POST['view'])){

            $username = $_POST['username'];
            
            $sql="SELECT * FROM user, profile, bmi 
                  WHERE user.username='$username'
                  AND profile.username='$username'
                  AND bmi.username='$username'
                  AND profile.role='User'";

            if ($result=mysqli_query($con,$sql)){
              // Fetch one and one row
              while ($row=mysqli_fetch_array($result)){
        ?>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card card-body pd-40">
                            <h5 class="card-title mg-b-20">Maklumat Pengguna</h5>

                            <form name="participant" action="delete.php" method="post">
                                <div class="form-group">
                                    <div class="row row-sm">
                                        <div class="col-sm-6 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Nama</label>
                                            <input type="text" name="name" value="<?= $row['name'] ?>" class="form-control">
                                        </div><!-- col -->
                                        <div class="col-sm-6 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Email</label>
                                            <input type="text" name="email" value="<?= $row['email'] ?>" class="form-control" >
                                        </div><!-- col -->
                                    </div><!-- row -->
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <div class="row row-sm">
                                        <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Jantina</label>
                                            <input type="text" name="gender" value="<?= $row['gender'] ?>" class="form-control" >
                                        </div><!-- col -->
                                        <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Berat (Kg)</label>
                                            <input type="text" name="weight" value="<?= $row['weight'] ?>" class="form-control" >
                                        </div><!-- col -->
                                        <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Tinggi (cm)</label>
                                            <input type="text" name="height" value="<?= $row['height'] ?>" class="form-control" >
                                        </div><!-- col -->
                                        <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Umur</label>
                                            <input type="text" name="age" value="<?= $row['age'] ?>" class="form-control" >
                                        </div><!-- col -->
                                    </div><!-- row -->
                                </div><!-- form-group -->

                                <button href="#modaldemo8" class="modal-effect btn btn-danger btn-block" data-toggle="modal" data-effect="effect-slide-in-right">Padam Pengguna</button>
                                <!-- MODAL EFFECTS -->
                                <div id="modaldemo8" class="modal">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content modal-content-demo">
                                            <div class="modal-header">
                                                <h6 class="modal-title">Padam Pengguna</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Anda pasti ingin padam maklumat pengguna ini? Semua maklumat yang berkaitan akan dipadam.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="username" value="<?= $row['username'] ?>"> 
                                                <button type="submit" name="deleteParticipant" class="btn btn-outline-success">Ya, Padam</button>
                                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div><!-- modal-dialog -->
                                </div><!-- modal -->
                            </form>
                        </div><!-- card -->
                    </div><!-- col -->
                </div><!-- row -->

        <?php
                    }
                }
            }
        ?>


      </div><!-- az-content-body -->

      <div class="az-footer">
        <?php include("config/footer.php"); ?>
      </div><!-- az-footer -->
    </div><!-- az-content -->


    <?php include("config/script.php"); ?>
    <script>
      $(function(){
        'use strict'

        // showing modal with effect
        $('.modal-effect').on('click', function(e){
          e.preventDefault();
          var effect = $(this).attr('data-effect');
          $('#modaldemo8').addClass(effect);
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

      });
    </script>
  </body>
</html>
