<?php 
session_start();
include("process/inc_db.php");
if(!isset($_SESSION['user'])){
  header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("config/head.php"); ?>
  </head>
  <body class="az-body az-body-sidebar">

    <?php include("config/sidemenu.php"); ?>

    <div class="az-content az-content-dashboard-two">

      <div class="az-content-header d-block d-md-flex">
        <div>
          <h2 class="az-content-title mg-b-5 mg-b-lg-8">Hi, Selamat Kembali <?= $_SESSION['user'] ?>!</h2>
          <p class="mg-b-0">Anda Sedang Melihat Papan Pemuka.</p>
        </div>
      </div><!-- az-content-header -->

      <div class="az-content-body">

      <?php

        if(isset($_POST['new_exercise'])) 
        {
            $type = $_POST['type'];
            $activity = $_POST['activity'];
            $mets = $_POST['mets'];

            //end input deposit or closed payment or other

            $sql = "INSERT INTO exercise(type, activity, mets)
                    VALUES ('$type', '$activity', '$mets')";

            if (mysqli_query($con, $sql)) {
    ?>
                <script>
                    setTimeout(function() {
                        swal({
                            title: "Aktiviti Berjaya Ditambah!",
                            type: "success",   
                            timer: 1000,   
                            showConfirmButton: false 
                        }).then(function() {
                            window.history.replaceState( null, null, window.location.href ); 
                        });
                    }, 1000);
                </script>
    <?php
            } else {
    ?>
                <script>
                    setTimeout(function() {
                        swal({
                            title: "Aouchhh!",
                            type: "error",   
                            timer: 1000,   
                            showConfirmButton: false 
                        }).then(function() {
                            window.history.replaceState( null, null, window.location.href ); 
                        });
                    }, 1000);
                </script>
    <?php
            }
        }

        if(isset($_POST['delete_exercise'])) 
        {

            $id = $_POST['id'];

            //end input deposit or closed payment or other

            $sql = "DELETE FROM exercise WHERE auto_id='$id'";

            if (mysqli_query($con, $sql)) {
    ?>
                <script>
                    setTimeout(function() {
                        swal({
                            title: "Aktiviti Berjaya Dipadam!",
                            type: "success",   
                            timer: 1000,   
                            showConfirmButton: false 
                        }).then(function() {
                            window.history.replaceState( null, null, window.location.href ); 
                        });
                    }, 1000);
                </script>
    <?php
            } else {
    ?>
                <script>
                    setTimeout(function() {
                        swal({
                            title: "Aouchhh!",
                            type: "error",   
                            timer: 1000,   
                            showConfirmButton: false 
                        }).then(function() {
                            window.history.replaceState( null, null, window.location.href ); 
                        });
                    }, 1000);
                </script>
    <?php
            }
        }
    ?>

      <a href="#modaldemo8" class="modal-effect btn btn-success btn-block" data-toggle="modal" data-effect="effect-slide-in-right">Tambah Senaman</a>
        <!-- MODAL EFFECTS -->
        <div id="modaldemo8" class="modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Senaman Baharu</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form name="exercise" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" name="type" placeholder="Close" required>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="text" class="form-control" name="activity" placeholder="Aktiviti, Ex: Umum / Pegun" required>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="number" min=".1" step=".1" class="form-control" name="mets" placeholder="Mets" required>
                            </div><!-- form-group -->
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="new_exercise" class="btn btn-indigo">Simpan</button>
                            <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <hr>
        <h2 class="az-content-title">Senarai Senaman</h2>
        <table id="datatable1" class="display responsive nowrap">
          <thead>
            <tr>
              <th class="wd-15p">Jenis</th>
              <th class="wd-20p">Aktiviti</th>
              <th class="wd-20p">Mets</th>
              <th class="wd-20p">Pilihan</th>
            </tr>
          </thead>
          <tbody>

          <?php
            $sql="SELECT * FROM exercise";

            if ($result=mysqli_query($con,$sql)){
              // Fetch one and one row
              while ($row=mysqli_fetch_array($result)){
          ?>
                <tr>
                  <td><?= $row['type'] ?></td>
                  <td><?= $row['activity'] ?></td>
                  <td><?= $row['mets'] ?></td>
                  <td>
                    <form name="exercise" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
                      <input type="hidden" name="id" value="<?= $row['auto_id'] ?>">
                      <button type="submit" name="delete_exercise" class="btn btn-xs btn-danger">Padam</button>
                    </form>
                  </td>
                </tr>
            <?php
                  }
              }
            ?>
          </tbody>
        </table>

      </div><!-- az-content-body -->

      <div class="az-footer">
        <?php include("config/footer.php"); ?>
      </div><!-- az-footer -->
    </div><!-- az-content -->

    <?php include("config/script.php"); ?>

    <script>
      $(document).ready(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Carian...',
            sSearch: '',
            lengthMenu: '_MENU_ item/halaman',
          }
        });
      });
    </script>
    <script>
      $(function(){
        'use strict'

        // showing modal with effect
        $('.modal-effect').on('click', function(e){
          e.preventDefault();
          var effect = $(this).attr('data-effect');
          $('#modaldemo8').addClass(effect);
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

      });
    </script>
  </body>
</html>
