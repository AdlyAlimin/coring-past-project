<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Azia">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/azia/img/azia-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/azia">
    <meta property="og:title" content="Azia">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <title>MyDipp Admin Dashboard</title>

    <!-- vendor css -->
    <link href="lib/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/typicons.font/typicons.css" rel="stylesheet">
    <link href="lib/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="lib/sweetalert2/dist/sweetalert2.all.min.js"></script>

    <!-- azia CSS -->
    <link rel="stylesheet" href="css/azia.css">

  </head>
  <body class="az-body">

    <?php
    session_start();
        include("process/inc_db.php");
        if(isset($_SESSION['user'])){
          header('Location: home.php');
        }

        if(isset($_POST['submit'])) 
        { 
            $username = $_POST['username'];
            $password = md5($_POST['password']);

            $sql="SELECT * FROM admin WHERE username='$username' AND password='$password'";
            $result=mysqli_query($con,$sql);

            // Associative array
            $row=mysqli_fetch_assoc($result);

            if($row>0)
            {
                $_SESSION['user'] = $row['name'];
                
                 header("Location: home.php");
            }else{
            ?>
                <script>
                    setTimeout(function() {
                        swal({
                            title: "Aouchhh!",
                            type: "error",   
                            timer: 1000,   
                            showConfirmButton: false 
                        }).then(function() {
                            window.history.replaceState( null, null, window.location.href ); 
                        });
                    }, 1000);
                </script>
            <?php
            }

            // Free result set
            mysqli_free_result($result);

            mysqli_close($con);
        }
    ?>

    <div class="az-signin-wrapper">
      <div class="az-card-signin">
        <h1 class="az-logo">My<span>Dipp</span></h1>
        <div class="az-signin-header">
          <h2>Selamat Datang!</h2>
          <h4>Sila Daftar Masuk</h4>

          <form name="signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
            <div class="form-group">
              <label>Nama Pengguna</label>
              <input type="text" class="form-control" name="username" placeholder="Masukkan Nama Pengguna" required>
            </div><!-- form-group -->
            <div class="form-group">
              <label>Kata Laluan</label>
              <input type="password" class="form-control" name="password" placeholder="Masukkan Kata Laluan" required>
            </div><!-- form-group -->
            <button type="submit" name="submit" class="btn btn-az-primary btn-block">Masuk</button>
          </form>
        </div><!-- az-signin-header -->
        <div class="az-signin-footer">
        </div><!-- az-signin-footer -->
      </div><!-- az-card-signin -->
    </div><!-- az-signin-wrapper -->

    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/ionicons/ionicons.js"></script>

    <script src="js/azia.js"></script>
    <script>
      $(function(){
        'use strict'

      });
    </script>
  </body>
</html>
