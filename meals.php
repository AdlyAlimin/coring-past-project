<?php 
session_start();
include("process/inc_db.php");
if(!isset($_SESSION['user'])){
  header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("config/head.php"); ?>
  </head>
  <body class="az-body az-body-sidebar">

    <?php include("config/sidemenu.php"); ?>

    <div class="az-content az-content-dashboard-two">

      <div class="az-content-header d-block d-md-flex">
        <div>
          <h2 class="az-content-title mg-b-5 mg-b-lg-8">Hi, Selamat Kembali <?= $_SESSION['user'] ?>!</h2>
          <p class="mg-b-0">Anda Sedang Melihat Papan Pemuka.</p>
        </div>
      </div><!-- az-content-header -->

      <?php

      if(isset($_GET['category']) && ($_GET['category']=="other")){
        include("meals/other.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="fish")){
        include("meals/fish.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="meat")){
        include("meals/meat.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="bread")){
        include("meals/bread.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="fat")){
        include("meals/fat.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="fruit")){
        include("meals/fruit.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="legume")){
        include("meals/legume.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="milk")){
        include("meals/milk.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="rice")){
        include("meals/rice.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="sweet")){
        include("meals/sweet.php");
      }else if(isset($_GET['category']) && ($_GET['category']=="vege")){
        include("meals/vege.php");
      }
      else{
        echo " Please Select Category ";
      }


      ?>

      <div class="az-footer">
        <?php include("config/footer.php"); ?>
      </div><!-- az-footer -->
    </div><!-- az-content -->


    <?php include("config/script.php"); ?>

    <script>
      $(document).ready(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Carian...',
            sSearch: '',
            lengthMenu: '_MENU_ item/halaman',
          }
        });
      });
    </script>
    <script>
        $('.editFood').on('click', function(){
          var currentRow=$(this).closest("tr");
          var col0=currentRow.find("td:eq(0)").text();
          var cat='<?php echo $_GET['category'];?>';
          $('#toEdit').load('modalEditMeals.php', { id: col0, edtExp: 'edit', cate: cat }, function() {
            $('#modalEdit').modal({
              show : true
            });
          });
        });
        
        // $('#editMeals').on('submit', function(e){
        //     e.preventDefault();
        //     var fd = new FormData(this);
        //     alert(fd);
        //     $.ajax({
        //         url:"modalEditMeals.php",
        //         type:"POST",
        //         data: fd,
        //         contentType: false,
        //         processData: false
        //     }).done(function(response){
        //         switch(response){
        //             case "":
        //                 alert("Material Added!");
        //                 // swal({
        //                 //     title: "Save!",
        //                 //     text: "UI Set",
        //                 //     type: "success",
        //                 //     showConfirmButton: false,
        //                 //     timer: 1300,
        //                 // }, function(){
        //                 //     window.location.reload();
        //                 // });
        //                 break;
        //             default:
        //                 alert("Fail to add Material!");
        //                 // swal({
        //                 //     title: "Warning!",
        //                 //     text: "There was an error, Please try again. If problem persist contact support",
        //                 //     type: "warning",
        //                 // }, function(){
        //                 //     window.location.reload();
        //                 // });
        //                 break;
        //         }
        //     }).fail(function(err){
        //         //swal("Error!", err, "error");
        //         alert("server error");
        //     });
        // });
        
      $(function(){
        'use strict'

        // showing modal with effect
        $('.modal-effect').on('click', function(e){
          e.preventDefault();
          var effect = $(this).attr('data-effect');
          $('#modaldemo8').addClass(effect);
        });

        // hide modal with effect
        $('#modaldemo8').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

      });
    </script>
  </body>
</html>
