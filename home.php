<?php 
session_start();
include("process/inc_db.php");

if(!isset($_SESSION['user'])){
  header('Location: index.php');
}
if(isset($_POST['new_exercise'])) 
{
  $email = $_POST['email'];
  $username = $_POST['username'];
  $password = $_POST['password'];

  $red = 0; $blue = 0; 
  $gcolor = mysqli_query($con,"SELECT * FROM profile");
  foreach ($gcolor as $colorvalue) {
    if($colorvalue['role'] === "Coach"){
      $colorvalue['groups'] !== 'red' ?: $red++ ;
      $colorvalue['groups'] !== 'blue' ?: $blue++ ;
    }
  }

  if ($red > $blue) {
    $assignGrup = "blue";
  }
  if($red < $blue){
    $assignGrup = "red";
  }
  if ($red === $blue) {
    $assignGrup = "red";
  }

  $sql = "INSERT INTO coach (username, email, password) 
        VALUES ('$username', '$email', '$password')";

  $sql2 = "INSERT INTO profile (username, role,groups) 
          VALUES ('$username', 'Coach', '$assignGrup')";

  // //end input deposit or closed payment or other
  // $sql = "INSERT INTO exercise(type, activity, mets)
  //         VALUES ('$type', '$activity', '$mets')";

  if (mysqli_query($con, $sql)) {
    if (mysqli_query($con, $sql2)) {
?>
    <script>
        setTimeout(function() {
            swal({
                title: "Coach Added!",
                type: "success",   
                timer: 1000,   
                showConfirmButton: false 
            }).then(function() {
                window.history.replaceState( null, null, window.location.href ); 
            });
        }, 1000);
    </script>
<?php
  } else {
?>
    <script>
        setTimeout(function() {
            swal({
                title: "Aouchhh!",
                type: "error",   
                timer: 1000,   
                showConfirmButton: false 
            }).then(function() {
                window.history.replaceState( null, null, window.location.href ); 
            });
        }, 1000);
    </script>
<?php
    }
  } else {
    ?>
        <script>
            setTimeout(function() {
                swal({
                    title: "Aouchhh!",
                    type: "error",   
                    timer: 1000,   
                    showConfirmButton: false 
                }).then(function() {
                    window.history.replaceState( null, null, window.location.href ); 
                });
            }, 1000);
        </script>
    <?php
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("config/head.php"); ?>
  </head>
  <body class="az-body az-body-sidebar">

    <?php include("config/sidemenu.php"); ?>

    <div class="az-content az-content-dashboard-two">

      <div class="az-content-header d-block d-md-flex">
        <div>
          <h2 class="az-content-title mg-b-5 mg-b-lg-8">Hi, Selamat Kembali <?= $_SESSION['user'] ?>!</h2>
          <p class="mg-b-0">Anda Sedang Melihat Papan Pemuka.</p>
        </div>
      </div><!-- az-content-header -->

      <div class="az-content-body">
        <hr>
        <h2 class="az-content-title">Senarai Peserta</h2>
        <table id="datatable1" class="display responsive nowrap">
          <thead>
            <tr>
              <th class="wd-15p">Nama</th>
              <th class="wd-15p">Jantina</th>
              <th class="wd-20p">Email</th>
              <th class="wd-15p">Kemaskini Profil Terakhir</th>
              <th class="wd-15p">Pilihan</th>
            </tr>
          </thead>
          <tbody>

          <?php
            $sql="SELECT * FROM user, profile, bmi 
                  WHERE user.username=profile.username
                  AND user.username=bmi.username
                  AND profile.role='User'";

            if ($result=mysqli_query($con,$sql)){
              // Fetch one and one row
              while ($row=mysqli_fetch_array($result)){
          ?>
                <tr>
                  <td><?= $row['name'] ?></td>
                  <td><?= $row['gender'] ?></td>
                  <td><?= $row['email'] ?></td>
                  <td><?= $row['update_date'] ?></td>
                  <td>
                    <form name="participant" action="participant.php" method="post">
                      <input type="hidden" name="username" value="<?= $row['username'] ?>">
                      <button type="submit" name="view" class="btn btn-xs btn-info">Lihat</button>
                    </form>
                  </td>
                </tr>
            <?php
                  }
              }
            ?>
          </tbody>
        </table>

        <hr>
        <h2 class="az-content-title">Senarai Jurulatih</h2>
        
        <a href="#modaldemo8" class="modal-effect btn btn-success btn-block" data-toggle="modal" data-effect="effect-slide-in-right">Daftar Jurulatih</a>
        <!-- MODAL EFFECTS -->
        <div id="modaldemo8" class="modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Jurulatih Baharu</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form name="exercise" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                              <input type="email" id="email" name="email" class="form-control rounded" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                              <input type="text" id="username" name="username" class="form-control rounded" placeholder="Nama Pengguna" required>
                            </div>
                            <div class="form-group">
                              <input type="password" id="password" name="password" class="form-control rounded" placeholder="Kata Laluan" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="new_exercise" class="btn btn-indigo">Daftar</button>
                            <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <br/><br/>

        <table id="datatable2" class="display responsive nowrap">
          <thead>
            <tr>
              <th class="wd-15p">Nama</th>
              <th class="wd-20p">Email</th>
              <th class="wd-20p">Jawatan</th>
              <th class="wd-20p">Pilihan</th>
            </tr>
          </thead>
          <tbody>

          <?php
            $sql="SELECT * FROM coach, profile 
                  WHERE coach.username=profile.username
                  AND profile.role='Coach'";

            if ($result=mysqli_query($con,$sql)){
              // Fetch one and one row
              while ($row=mysqli_fetch_array($result)){
          ?>
                <tr>
                  <td><?= $row['name'] ?></td>
                  <td><?= $row['email'] ?></td>
                  <td><?= $row['position'] ?></td>
                  <td>
                    <form name="coach" action="coach.php" method="post">
                      <input type="hidden" name="username" value="<?= $row['username'] ?>">
                      <button type="submit" name="view" class="btn btn-xs btn-info">Lihat</button>
                    </form>
                  </td>
                </tr>
            <?php
                  }
              }
            ?>
          </tbody>
        </table>

      </div><!-- az-content-body -->

      <div class="az-footer">
        <?php include("config/footer.php"); ?>
      </div><!-- az-footer -->
    </div><!-- az-content -->

    <?php
      if(isset($_GET['id'])=="1"){
    ?>
        <script>
            setTimeout(function() {
                swal({
                    title: "DiPadam!",
                    type: "success",   
                    timer: 1000,   
                    showConfirmButton: false 
                }).then(function() {
                    window.history.replaceState( null, null, "home.php" ); 
                });
            }, 1000);
        </script>
    <?php
      } 
    ?>


    <?php include("config/script.php"); ?>

    <script>
      $(document).ready(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Carian...',
            sSearch: '',
            lengthMenu: '_MENU_ item/halaman',
          }
        });

        $('#datatable2').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Carian...',
            sSearch: '',
            lengthMenu: '_MENU_ item/halaman',
          }
        });
      });
    </script>
  </body>
</html>
