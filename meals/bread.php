<div class="az-content-body">

    <?php

    if(isset($_POST['new_meals'])) 
    { 

        $name = $_POST['name'];
        $measure = $_POST['measure'];
        $energy = $_POST['energy'];
        $carbo = $_POST['carbo'];
        $protein = $_POST['protein'];
        $fat = $_POST['fat'];
        $simple_sugar = $_POST['simple_sugar'];

        //end input deposit or closed payment or other

        $sql = "INSERT INTO bread(name, measure, energy, carbo, protein, fat, simple_sugar)
                VALUES ('$name', '$measure', '$energy', '$carbo', '$protein', '$fat', '$simple_sugar')";

        if (mysqli_query($con, $sql)) {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Makanan Berjaya Ditambah!",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        } else {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Aouchhh!",
                        type: "error",   
                        timer: 1000,   
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        }
    }

    if(isset($_POST['delete'])) 
    { 

        $id = $_POST['id'];

        //end input deposit or closed payment or other

        $sql = "DELETE FROM bread WHERE id='$id'";

        if (mysqli_query($con, $sql)) {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Makanan Berjaya Dipadam!",
                        type: "success",   
                        timer: 1000,   
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        } else {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Aouchhh!",
                        type: "error",   
                        timer: 1000,   
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        }
    }
    
    if(isset($_POST['edit_meals'])) 
    {
        $id = $_POST['id'];

        //end input deposit or closed payment or other

        $sql = "UPDATE bread 
            SET name='".$_POST['name']."',measure='".$_POST['measure']."',energy='".$_POST['energy']."',carbo='".$_POST['carbo']."',protein='".$_POST['protein']."',fat='".$_POST['fat']."',simple_sugar='".$_POST['simple_sugar']."' 
            WHERE id='$id'";

        if (mysqli_query($con, $sql)) {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Makanan Berjaya Diubah!",
                        type: "success",   
                        timer: 1000,   
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        } else {
    ?>
            <script>
                setTimeout(function() {
                    swal({
                        title: "Aouchhh!",
                        type: "error",   
                        timer: 1000,   
                        showConfirmButton: false 
                    }).then(function() {
                        window.history.replaceState( null, null, window.location.href ); 
                    });
                }, 1000);
            </script>
    <?php
        }
    }
    ?>

    <a href="#modaldemo8" class="modal-effect btn btn-success btn-block" data-toggle="modal" data-effect="effect-slide-in-right">Tambah Makanan</a>
    <!-- new meals add -->
    <!-- MODAL EFFECTS -->
    <div id="modaldemo8" class="modal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Makanan Baharu</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']."?category=bread"); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Nama Makanan" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="text" class="form-control" name="measure" placeholder="Measure, Ex: 1 Cup or 1/2 Bowl or 1 Table Spoon" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="number" min="0" step="0.000001" class="form-control" name="energy" placeholder="Tenaga" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="number" min="0" step="0.000001" class="form-control" name="carbo" placeholder="Karbohidrat" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="number" min="0" step="0.000001" class="form-control" name="protein" placeholder="Protein" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="number" min="0" step="0.000001" class="form-control" name="fat" placeholder="Lemak" required>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="number" min="0" step="0.000001" class="form-control" name="simple_sugar" placeholder="Gula Ringkas" required>
                        </div><!-- form-group -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="new_meals" class="btn btn-indigo">Simpan</button>
                        <button type="reset" class="btn btn-outline-light" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div id="modalEdit" class="modal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Ubah Maklumat Makanan</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="edit" id="editMeals" action="<?php echo htmlentities($_SERVER['PHP_SELF']."?category=bread"); ?>" method="post">
                    <div id="toEdit" class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="edit_meals" class="btn btn-indigo">Simpan</button>
                        <button type="reset" class="btn btn-outline-light" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <hr>
    <h2 class="az-content-title">Senarai Makanan berdasarkan Roti</h2>
    <table id="datatable1" class="display responsive nowrap">
        <thead>
        <tr>
            <th class="wd-15p">Nama</th>
            <th class="wd-15p">Kuantiti</th>
            <th class="wd-15p">Tenaga</th>
            <th class="wd-15p">Karbohidrat</th>
            <th class="wd-15p">Protein</th>
            <th class="wd-15p">Lemak</th>
            <th class="wd-15p">Gula Ringkas</th>
            <th class="wd-15p">Pilihan</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $sql="SELECT * FROM bread";

        if ($result=mysqli_query($con,$sql)){
            // Fetch one and one row
            while ($row=mysqli_fetch_array($result)){
        ?>
            <tr>
                <td><?= $row['name'] ?></td>
                <td><?= $row['measure'] ?></td>
                <td><?= $row['energy'] ?> kcal</td>
                <td><?= $row['carbo'] ?> g</td>
                <td><?= $row['protein'] ?> g</td>
                <td><?= $row['fat'] ?> g</td>
                <td><?= $row['simple_sugar'] ?> g</td>
                <td>
                    <form name="signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']."?category=bread"); ?>" method="post">
                        <input type="hidden" name="id" value="<?= $row['id'] ?>">
                        <button type="submit" name="delete" class="btn btn-xs btn-danger">Padam</button>
                    </form>
                    <br>
                    <button type="button" name="edit" class="btn btn-xs btn-info editFood">Ubah</button>
                </td>
            </tr>
        <?php
                }
            }
        ?>
        </tbody>
    </table>

</div><!-- az-content-body -->